<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="description" content="Page description. No longer than 155 characters." />
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="The Name or Title Here">
    <meta itemprop="description" content="This is the page description">
    <meta itemprop="image" content="http://www.example.com/image.jpg">
    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@publisher_handle">
    <meta name="twitter:title" content="Page Title">
    <meta name="twitter:description" content="Page description less than 200 characters">
    <meta name="twitter:creator" content="@author_handle">
    <!-- Twitter summary card with large image must be at least 280x150px -->
    <meta name="twitter:image:src" content="http://www.example.com/image.jpg">
    <!-- Open Graph data -->
    <meta property="og:title" content="Title Here" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="http://www.example.com/" />
    <meta property="og:image" content="http://example.com/image.jpg" />
    <meta property="og:description" content="Description Here" />
    <meta property="og:site_name" content="Site Name, i.e. Moz" />
    <meta property="article:published_time" content="" />
    <meta property="article:modified_time" content="" />
    <meta property="article:section" content="Article Section" />
    <meta property="article:tag" content="Article Tag" />
    <meta property="fb:admins" content="Facebook numberic ID" />
    <!-- fav icon -->
    <link rel="apple-touch-icon-precomposed" href="images/fav-icons/fav-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="">
    <link rel="shortcut icon" href="assets/images/fav-icons/favicon48x48.ico">
    <link rel="shortcut icon" href="assets/images/fav-icons/fav-icon.png">
    <link rel="shortcut icon" href="assets/images/fav-icons/fav-icon128x128.png">
    <title> Home </title>
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/custom-style.css">
    </head>

<body>
    <!--  / header /  -->
    <header>
    </header>
     <!--  / footer /  -->
    <footer>
    </footer>
    
   <!-- scripts -->
     <script src="assets/js/jquery-v3.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/js/custom-scripts.js" type="text/javascript"></script>
    </body>
</html>